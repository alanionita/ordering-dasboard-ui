import React from "react";
import Styled from "./index.styled";
import Typography from "../Typography.styled";

export default ({ current, last }) => (
  <Styled.PanelIndicator>
    <Typography.Pagination active={true}>{current}</Typography.Pagination>
    <Styled.Separator />
    <Typography.Pagination>{last}</Typography.Pagination>
  </Styled.PanelIndicator>
);
