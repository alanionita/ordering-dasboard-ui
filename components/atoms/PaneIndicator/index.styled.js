import styled from "styled-components";
import colors from "../../../styles/settings/colors";

const PanelIndicator = styled.div`
  position: absolute;
  right: 32px;
  bottom: 32px;
  height: 40px;
  width: auto;
  display: flex;
  flex-flow: row;
  justify-content: center;
  align-items: center;
`;

const Separator = styled.hr`
  width: 1px;
  height: 100%;
  margin: 0 8px;
  background-color: ${colors.lightGrey};
  padding: 0;
  border: 0;
`;

export default {
  PanelIndicator,
  Separator
};
