import styled from "styled-components";

export const StyledImageThumbnail = styled.figure`
  height: 80px;
  width: 80px;
  margin: 0;
  margin-right: 16px;
  border-radius: 8px;
  & > picture {
    height: 100%;
    width: 100%;
    & > img {
      display: block;
      max-width: 80px;
      max-height: 80px;
      border-radius: 8px;
    }
  }
`;
