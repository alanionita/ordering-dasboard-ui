import React from "react";

import { StyledImageThumbnail } from "./index.styled";

export default ({ src, alt }) => (
  <StyledImageThumbnail>
    <picture>
      <source srcSet={`${src}/160 300w, ${src}/320 768w, ${src}/480 1280w`} />
      <img src={`${src}/160`} alt={alt} />
    </picture>
  </StyledImageThumbnail>
);
