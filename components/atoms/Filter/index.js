import React, { useState } from "react";

// Components
import Styled from "./index.styled";
import Typography from "../Typography.styled";

// Data
import DataContext from "../../../lib/context/dataContext";
import FiltersContext from "../../../lib/context/filtersContext";
import actions from "../../../lib/actions";

export default ({ Status, Title }) => {
  const [state, dispatch] = DataContext.useDataContext();
  const [filtersState, filtersDispatch] = FiltersContext.useFiltersContext();
  const handleOnClick = () => {
    if (!filtersState.activeFilter || filtersState.activeFilter !== Status) {
      dispatch(actions.filterSourceByStatus(state.sourceData, Status));
      dispatch(actions.setActivePane(0));
      filtersDispatch(actions.setActiveFilter(Status));
    }
    if (filtersState.activeFilter === Status) {
      dispatch(actions.filterSourceByStatus(state.sourceData));
      filtersDispatch(actions.setActiveFilter(null));
    }
  };

  return (
    <li>
      <Styled.TappableArea
        aria-pressed={filtersState.activeFilter === Status}
        onClick={handleOnClick}>
        <Styled.FilterPip color={Status} />
        <Typography.FilterLinks>{Title}</Typography.FilterLinks>
      </Styled.TappableArea>
    </li>
  );
};
