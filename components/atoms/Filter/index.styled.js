import styled from "styled-components";
import colors from "../../../styles/settings/colors";

const FilterPip = styled.span`
  display: block;
  height: 16px;
  width: 16px;
  border-radius: 50%;
  border: 1px solid ${colors.lightGrey};
  background-color: ${({ color }) => colors[color] || colors.lightGrey};
  margin-right: 8px;
`;

const TappableArea = styled.button`
  background-color: transparent;
  border: none;
  display: flex;
  flex-flow: row nowrap;
  justify-content: center;
  align-items: center;
  &:hover {
    cursor: pointer;
  }
  
  &:focus {
    outline: 2px solid ${colors.green};
  }
  &[aria-pressed='true'] {
    background-color: rgba(0, 0, 0, 0.5);
    border-radius: 8px;
    border: 1px solid ${colors.lightGrey}
  }
`;

export default { FilterPip, TappableArea };
