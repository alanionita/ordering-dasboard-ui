import styled from "styled-components";
import colors from "../../styles/settings/colors";
const Title = styled.h1`
  color: ${colors.lightGrey};
  font-size: 16px;
  font-weight: 600;
`;

const Subtitle = styled.h2`
  color: ${colors.darkGrey};
  font-size: 16px;
  font-weight: 600;
`;

const FilterLinks = styled.p`
  color: ${colors.lightGrey};
  font-size: 8px;
  font-weight: 600;
  text-transform: uppercase;
`;

const Pagination = styled.p`
  color: ${colors.lightGrey};
  color: ${({ active }) => (active ? colors.darkGrey : colors.lightGrey)}
  font-size: 16px;
  font-weight: ${({ active }) => (active ? "600" : "300")};
  text-transform: uppercase;
`;

export default {
  Title,
  Subtitle,
  FilterLinks,
  Pagination
};
