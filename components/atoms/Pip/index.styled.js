import styled from "styled-components";
import colors from "../../../styles/settings/colors";

const Pip = styled.span`
  display: block;
  height: 16px;
  width: 16px;
  border-radius: 50%;
  background-color: ${({ active }) =>
    active ? colors.green : colors.lightGrey};
`;

const TappableArea = styled.button`
  display: flex;
  flex-flow: row wrap;
  justify-content: center;
  align-items: center;
  height: 40px;
  width: 40px;
  background-color: transparent;
  border: none;
  box-shadow: none;

  &:hover {
    & > span {
      background-color: ${colors.darkGrey};
    }
  }
  &:active,
  &:focus {
    & > span {
      background-color: ${colors.green};
    }
    outline: 1px solid ${colors.green};
  }
`;

export default {
  Pip,
  TappableArea
};
