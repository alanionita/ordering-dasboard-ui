import React from "react";
import Styled from "./index.styled";

export default ({ active, index, handleOnClick }) => {
  return (
    <Styled.TappableArea onClick={() => handleOnClick()}>
      <Styled.Pip active={active} key={`carousel-dot--${index}`} />
    </Styled.TappableArea>
  );
};
