import styled from "styled-components";
import colors from "../../../styles/settings/colors";

const FilterRow = styled.ul`
  padding: 0;
  list-style-type: none;
  width: auto;
  margin: 24px 0;
  height: 40px;
  display: flex;
  flex-flow: row;
  justify-content: flex-end;
  align-items: center;
  & > * {
    padding: 0 16px;
  }
`;

const FilterPip = styled.span`
  display: block;
  height: 16px;
  width: 16px;
  border-radius: 50%;
  border: 1px solid ${colors.lightGrey};
  background-color: ${({ color }) => colors[color] || colors.lightGrey};
`;

export default { FilterRow, FilterPip };
