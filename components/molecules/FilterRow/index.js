import React from "react";

// Components
import Styled from "./index.styled";
import Filter from "../../atoms/Filter";

// Data
import filterData from "../../../data/filters.json";

export default () => {
  return (
    <Styled.FilterRow>
      {filterData.map(filter => (
        <Filter {...filter} key={filter.ID} />
      ))}
    </Styled.FilterRow>
  );
};
