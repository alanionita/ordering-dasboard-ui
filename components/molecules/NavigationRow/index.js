import React from "react";

// Components
import Styled from "./index.styled";
import Pip from "../../atoms/Pip";
import PanelIndicator from "../../atoms/PaneIndicator";

// Context
import DataContext from "../../../lib/context/dataContext";
import actions from "../../../lib/actions";

export default () => {
  const [state, dispatch] = DataContext.useDataContext();
  return (
    <Styled.NavigationRow__Container>
      <Styled.NavigationRow>
        {state &&
          state.data.map((_, index) => (
            <li key={`navigation-row__dot--${index}`}>
              <Pip
                index={index}
                active={index === state.activePane}
                handleOnClick={() => dispatch(actions.setActivePane(index))}
              />
            </li>
          ))}
      </Styled.NavigationRow>
      <PanelIndicator
        current={state && state.activePane + 1}
        last={state && state.data.length}
      />
    </Styled.NavigationRow__Container>
  );
};
