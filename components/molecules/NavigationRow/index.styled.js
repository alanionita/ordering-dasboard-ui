import styled from "styled-components";
import Container from "../../atoms/Container.styled";

const NavigationRow = styled.ul`
  padding: 0;
  list-style-type: none;
  width: auto;
  margin: 32px auto;
  display: flex;
  flex-flow: row;
  justify-content: center;
  align-items: center;
  position: relative;
`;

const NavigationRow__Container = styled(Container)`
  position: relative;
  bottom: 0;
  left: 0;
  right: 0;
  width: 100%;
`;

export default { NavigationRow, NavigationRow__Container };
