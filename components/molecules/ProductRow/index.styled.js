import styled from "styled-components";

// Styled components
import Typography from "../../atoms/Typography.styled";

// Variables
import colors from "../../../styles/settings/colors";

// Styles
const ListItem = styled.li`
  display: flex;
  flex-flow: row nowrap;
  align-items: center;
  min-height: 100px;
  border: 1px solid ${colors.darkGrey};
  border-radius: 8px;
  margin-bottom: 16px;
  padding: 8px 16px 8px 32px;
  position: relative;
  flex: 0 1 calc(100% / 4 - 16px); 
  &:before {
    content: "";
    position: absolute;
    height: 100%;
    width: 18px;
    background-color: ${({ status }) => colors[status] || colors.darkGrey};
    left: 0;
    border-radius: 8px 0px 0px 8px;
  }
  &:last-child {
    margin-bottom: 0px;
  }
`;

const TextContainers = styled.section`
  display: flex;
  flex-flow: row nowrap;
  justify-content: space-between;
  align-items: center;
  flex: 1;
  position: relative;
  & > * {
    flex: 1 1 25%;
  }
`;

const InitialContainer = styled.div`
  border: 1px solid ${colors.lightGrey};
  padding: 0 auto;
  flex: 0 0 40px;
  height: 40px;
  border-radius: 8px;
  display: flex;
  flex-flow: row nowrap;
  justify-content: center;
  align-items: center;
`;

const ProductRowTitle = styled(Typography.Title)`
  max-width: 92px;
  line-height: 1;
`;

const ProductRowSubtitle = styled(Typography.Subtitle)`
  max-width: 92px;
  line-height: 1;
`;

export default {
  ListItem,
  TextContainers,
  InitialContainer,
  Typography: {
    Title: ProductRowTitle,
    Subtitle: ProductRowSubtitle
  }
};
