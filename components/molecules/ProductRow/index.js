import React from "react";
import Styled from "./index.styled";
import ImageThumbnail from "../../atoms/ImageThumbnail";

export default ({
  Product_Name,
  Category,
  Size,
  Color,
  Customer_initials,
  Status,
  Product_Thumbnail
}) => (
  <Styled.ListItem status={Status}>
    <ImageThumbnail src={Product_Thumbnail} alt={Product_Name} />
    <Styled.TextContainers>
      <Styled.Typography.Title>{Product_Name}</Styled.Typography.Title>
      <Styled.Typography.Subtitle>
        Category: {Category}
      </Styled.Typography.Subtitle>
      <Styled.Typography.Subtitle>Size: {Size}</Styled.Typography.Subtitle>
      <Styled.Typography.Subtitle>Color: {Color}</Styled.Typography.Subtitle>
      <Styled.InitialContainer>
        <Styled.Typography.Subtitle>
          {Customer_initials}
        </Styled.Typography.Subtitle>
      </Styled.InitialContainer>
    </Styled.TextContainers>
  </Styled.ListItem>
);
