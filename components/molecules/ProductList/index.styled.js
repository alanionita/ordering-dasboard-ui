import styled from "styled-components";
import Container from "../../atoms/Container.styled";

const List = styled.ul`
  padding: 0;
  list-style-type: none;
  width: 100%;
  margin: 0;
  height: 100%;
  display: flex;
  flex-flow: column;
`;

const ListContainer = styled(Container)`
  height: 100%;
`

export default {
  List, ListContainer
};
