import React from "react";

// Components
import ProductRow from "../ProductRow";
import Styled from "./index.styled";

// Context
import DataContext from "../../../lib/context/dataContext";

export default () => {
  const [state, _] = DataContext.useDataContext();
  return (
    <Styled.ListContainer>
      <Styled.List>
        {state &&
          state.data[state.activePane] &&
          state.data[state.activePane].map(dataRow => (
            <ProductRow {...dataRow} key={dataRow.id} />
          ))}
      </Styled.List>
    </Styled.ListContainer>
  );
};
