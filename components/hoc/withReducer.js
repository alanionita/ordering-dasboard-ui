import React, { useReducer } from "react";

export default (WrappedComponent, redux) => {
  const { Provider } = WrappedComponent;
  return props => {
    const [state, dispatch] = useReducer(redux.reducer, redux.initialState);
    return <Provider value={[state, dispatch]}>{props.children}</Provider>;
  };
};
