import styled from "styled-components";
import colors from "../../../styles/settings/colors";

export const StyledApp = styled.main`
  display: flex;
  flex-flow: column;
  padding: 0 32px;
  background-color: #1f1f1f;
  height: 100vh;
  width: 100%;
  position: relative;
`;

