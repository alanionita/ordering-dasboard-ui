import React, { useEffect, useState } from "react";

// Components
import FilterList from "../../molecules/FilterRow";
import NavigationRow from "../../molecules/NavigationRow";
import ProductList from "../../molecules/ProductList";
import { StyledApp } from "./index.styled";

// Data
import DataContext from "../../../lib/context/dataContext.js";
import FiltersContext from "../../../lib/context/filtersContext";
import actions from "../../../lib/actions";

export default ({ data }) => {
  const [state, dispatch] = DataContext.useDataContext();
  useEffect(() => {
    dispatch(actions.setData(data));
    dispatch(actions.setSourceData(data));
  }, data);
  const [currentCount, setCount] = useState(10);
  const timer = () => setCount(currentCount - 1 >= 0 ? currentCount - 1 : 10);

  useEffect(() => {
    const id = setInterval(timer, 10000);
    dispatch(
      actions.setActivePane(
        state.activePane + 1 <= state.data.length - 1 ? state.activePane + 1 : 0
      )
    );
    return () => clearInterval(id);
  }, [currentCount]);
  return (
    <StyledApp>
      <FiltersContext.Provider>
        <FilterList />
      </FiltersContext.Provider>
      <ProductList />
      <NavigationRow />
    </StyledApp>
  );
};
