import fetch from "isomorphic-fetch";
import React from "react";

import Head from "next/head";
import App from "../components/organisms/App";
import GlobalStyle from "../styles/global.styled";

// Data
import DataContext from "../lib/context/dataContext.js";

const Home = ({ data }) => {
  return (
    <React.Fragment>
      <GlobalStyle />
      <DataContext.Provider>
        <App data={data} />
      </DataContext.Provider>
    </React.Fragment>
  );
};

export async function getServerSideProps(context) {
  const res = await fetch(process.env.API_URL);
  const json = await res.json();
  return {
    props: {
      data: json
    }
  };
}

export default Home;
