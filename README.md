# Ordering Dashboard

![](https://media.giphy.com/media/Jp5NAO6PGtWAwfeuHl/giphy.gif)

Serverless ordering dashboard using NextJS

Links:
- [Zeit Now Serverless :: with API data ✅](https://ordering-dasboard-da62pe3z4.now.sh/)
- [Cloudfront Lambda@Edge :: with API data, but currently broken ❌](https://d3v2tbuj43s349.cloudfront.net/)
- [Cloudfront Lambda@Edge :: with static data ✅](https://d1kudfffo0ysai.cloudfront.net/)

## User stories 

Audience: shoe shop employee would like to manage all the orders happening on the shop floor, with a connection to the store room.

```
As a shop assistant I can see a list of the order queue, so that I can see the orders as they are placed (latest first)
```

```
As a shop assistant I can filter the list by order status, so that I can check up orders as customers ask
```

```
As a shop assistant I can remove the filter, so that I can see the full list
```

```
As a shop shop manager I want the app to rotate panels, so that I reuse the app as a display for customers
```

```
As a shop shop manager I expect the app to feature an API, so that orders are securely stored in the cloud
```

```
As a shop shop manager I expect the app to feature an API, so that new orders can be created
```

```
As a shop shop manager I expect the app to feature an API, so that existing orders can be removed
```

## Architecture and Rationale

![](https://raw.githubusercontent.com/danielcondemarin/serverless-next.js/master/arch_no_grid.png)

NextJS deployed on serveless functions with fetch requests going to another serverless api.

For the challenge I wanted to also use serverless framework in order to speed up development. In the same vain I use serverless components to further speed up the orchestration and reduce setup. 

Because of some deployment challenges (related to serverless components), I also introduced Zeit as a hosting platform.

### Design 

The original design was loaded in Figma and each component was sliced as required. 

I wanted to maintain visual consistency so I picked an 8pt grid and rounded each dimension to match that.

### Frontend

With a base in React I realised early on that a design system might be required so I use atomic design to split up my components. I tried to maintain abstraction without overdoing it. 

From the early stages of the project it was obvious that state was required and so I implemented a reducer pattern using the Context API and Hooks.

Higher order components were used to further abstract logic and improve readability and comprehension.

Styled-components were used for applying styles, because I imagined that there might be potential here for a native app.

## Challenges and Lessons learned

- The serverless-next.js component has been great for quickly spining up the architecture, but when I came to setting and using env variables the build failed. To avoid any further delays I switched the deployment to Zeit.
- Related to the previous point I originally hoped to build a NextJS monolith that contained both the front and backend, but the serverless-next.js component setup made it difficult to debug, so eventually I adandoned the idea and decouple the frontend from the API and DynamoDB
- No clear brief on how the filters should work so I implemented a solution that allows users to cycle through the filters and when you click again on a filter you return to the previous list. The clarity of this feature was hinging on correct hover and toggle styles so I reuse the existing design token and create my own.
- Using NextJS 9.3 enabled me to try the new [getServerSideProps](https://nextjs.org/docs/basic-features/data-fetching#getserversideprops-server-side-rendering), but it took a bit of experimentation to find the best way to add that data to existing contexts present in the app. Ultimately I settled on using a mixture of props, Provider lifting, and a new set of reducer actions.
- Not a new issue for NextJS, but the app wasn't loading styles so implemented a custome _document in order to improve that.

## Installation
-------------

With [npm](https://npmjs.org/) installed, run

```
$ npm i
```

## Developing
-------------

To run the app locally, execute ⬇️. If this is the first time running the app you may need to run the build script first.

```
npm run dev
```

This triggers the NextJS hot-reload server too so you'll be able to see changes in real-time

### Building

```
npm run build
```


## API
-------------

Uses the API described [here](https://gitlab.com/alanionita/ordering-dasboard-api) to render data

## Acknowledgments
-------------

ordering-dashboard was inspired by.... [`danielcondemarin/serverless-next.js`](https://github.com/danielcondemarin/serverless-next.js)

## See Also
-------------

- [`noffle/common-readme`](https://github.com/noffle/common-readme)
- [`ichyr/nextjs-with-styled-components`](https://github.com/ichyr/nextjs-with-styled-components)
- [`ichyr/nextjs-with-styled-components`](https://github.com/ichyr/nextjs-with-styled-components)
- [`next.js/examples/with-dotenv/`](https://github.com/zeit/next.js/tree/canary/examples/with-dotenv)

## License

ISC
