import types from "./types";
// Utils
import { filterByStatus } from "../utils";

const setActivePane = index => ({
  type: types.SET_ACTIVE_PANE,
  payload: index
});

const setActiveFilter = status => ({
  type: types.SET_ACTIVE_FILTER,
  payload: status
});

const filterSourceByStatus = (source, status = null) => {
  return {
    type: types.FILTER_DATA_BY,
    payload: status ? filterByStatus(source, status) : source
  };
};

const setData = data => {
  return { type: types.SET_DATA, payload: data };
};

const setSourceData = data => {
  return { type: types.SET_SOURCE_DATA, payload: data };
};

export default {
  setActivePane,
  filterSourceByStatus,
  setActiveFilter,
  setData,
  setSourceData
};
