import React, { useContext, createContext } from "react";
import withReducer from "../../components/hoc/withReducer";
import dataReducer from "../reducers/dataReducer";

export const DataContext = createContext();

const Provider = withReducer(DataContext, dataReducer);

const useDataContext = () => {
  return useContext(DataContext);
};

export default { Provider, useDataContext };
