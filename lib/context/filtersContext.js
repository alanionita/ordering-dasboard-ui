import React, { useContext, useReducer, createContext } from "react";
import withReducer from "../../components/hoc/withReducer";
import filtersReducer from "../reducers/filtersReducer";

export const FiltersContext = createContext();

const Provider = withReducer(FiltersContext, filtersReducer)

const useFiltersContext = () => {
  return useContext(FiltersContext);
};

export default { Provider, useFiltersContext };
