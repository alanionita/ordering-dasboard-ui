import types from "../actions/types";

const initialState = {
  activeFilter: null
};

const reducer = (state, action) => {
  switch (action.type) {
    case types.SET_ACTIVE_FILTER:
      return { ...state, activeFilter: action.payload };
    default:
      return;
  }
};

export default {
  reducer,
  initialState
};
