// Data
import queueData from "../../data/queue.json";
import { chunkData } from "../utils.js";
import types from "../actions/types";

const initialState = {
  data: [],
  activePane: 0,
  sourceData: []
};

const reducer = (state, action) => {
  switch (action.type) {
    case types.SET_ACTIVE_PANE:
      return { ...state, activePane: action.payload };
    case types.FILTER_DATA_BY:
      return { ...state, data: chunkData(action.payload, 4) };
    case types.SET_DATA:
      return { ...state, data: chunkData(action.payload, 4) };
    case types.SET_SOURCE_DATA:
      return { ...state, sourceData: action.payload };
    default:
      return;
  }
};

export default {
  reducer,
  initialState
};
