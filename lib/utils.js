
export const chunkData = (source, chunkSize) =>
  source.reduce(
    (acc, _, index, origArr) =>
      index % chunkSize
        ? acc
        : [...acc, origArr.slice(index, index + chunkSize)],
    []
  );

export const filterByStatus = (source, label) =>
  source.filter(elem => elem.Status === label);
