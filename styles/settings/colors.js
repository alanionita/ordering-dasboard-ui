export default {
  lightGrey: "#fdfdfd",
  darkGrey: "#9e9f94",
  green: "#48D5B0",
  blue: "#60ACED",
  orange: "#FD6935",
  red: "#EA0B00"
};
